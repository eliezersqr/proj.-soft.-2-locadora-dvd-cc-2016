package locadora.control;

import locadora.view.LoginWindow;

public class MainAplication {

	static private MainAplication app;
		
	public static void main(String[] args) {
		getApp();

	}
	
	private MainAplication(){
		new LoginWindow();
	}

	static public MainAplication getApp(){
		if(app == null)
			app = new MainAplication();
			
		return app;
	}

}