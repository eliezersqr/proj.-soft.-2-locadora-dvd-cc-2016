package locadora.model;

public class Cartao {
	
	private String tipoCliente;
	private Integer creditos;
	
	public Cartao(String tipoCliente, Integer creditos) {
		super();
		this.tipoCliente = tipoCliente;
		this.creditos = creditos;
	}
	
	public String getTipoCliente() {
		return tipoCliente;
	}
	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	public Integer getCreditos() {
		return creditos;
	}
	public void setCreditos(Integer creditos) {
		this.creditos = creditos;
	}

	@Override
	public String toString() {
		return "Cartao [tipoCliente=" + tipoCliente + ", creditos=" + creditos + "]";
	}
	
}