package locadora.model;

public class Status_Ator {
	
	private int status;

	public Status_Ator(int status) {
		super();
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Status_Ator [status=" + status + "]";
	}
	
}