package locadora.model;

public class Generos {

	private String nome;

	public Generos(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Generos [nome=" + nome + "]";
	}
	
}