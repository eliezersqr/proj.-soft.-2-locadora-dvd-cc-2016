package locadora.model;

public class Itens_Locacao {

	private int cdLocacao;
	private int cdFilme;
	private int sub_total;
	
	public Itens_Locacao(int cdLocacao, int cdFilme, int sub_total) {
		super();
		this.cdLocacao = cdLocacao;
		this.cdFilme = cdFilme;
		this.sub_total = sub_total;
	}
	public int getCdLocacao() {
		return cdLocacao;
	}
	public void setCdLocacao(int cdLocacao) {
		this.cdLocacao = cdLocacao;
	}
	public int getCdFilme() {
		return cdFilme;
	}
	public void setCdFilme(int cdFilme) {
		this.cdFilme = cdFilme;
	}
	public int getSub_total() {
		return sub_total;
	}
	public void setSub_total(int sub_total) {
		this.sub_total = sub_total;
	}

	@Override
	public String toString() {
		return "Itens_Locacao [cdLocacao=" + cdLocacao + ", cdFilme=" + cdFilme + ", sub_total=" + sub_total + "]";
	}
	
}