package locadora.model;

public class Locacao {
	
	private String dataLocacao;
	private String dataMaxDevolucao;
	private int valor_total;
	private String dataDevolucao;
	private int devolvido;
	
	public Locacao(String dataLocacao, String dataMaxDevolucao, int valor_total, String dataDevolucao, int devolvido) {
		super();
		this.dataLocacao = dataLocacao;
		this.dataMaxDevolucao = dataMaxDevolucao;
		this.valor_total = valor_total;
		this.dataDevolucao = dataDevolucao;
		this.devolvido = devolvido;
	}
	
	public String getDataLocacao() {
		return dataLocacao;
	}
	public void setDataLocacao(String dataLocacao) {
		this.dataLocacao = dataLocacao;
	}
	public String getDataMaxDevolucao() {
		return dataMaxDevolucao;
	}
	public void setDataMaxDevolucao(String dataMaxDevolucao) {
		this.dataMaxDevolucao = dataMaxDevolucao;
	}
	public int getValor_total() {
		return valor_total;
	}
	public void setValor_total(int valor_total) {
		this.valor_total = valor_total;
	}
	public String getDataDevolucao() {
		return dataDevolucao;
	}
	public void setDataDevolucao(String dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}
	public int getDevolvido() {
		return devolvido;
	}
	public void setDevolvido(int devolvido) {
		this.devolvido = devolvido;
	}

	@Override
	public String toString() {
		return "Locacao [dataLocacao=" + dataLocacao + ", dataMaxDevolucao=" + dataMaxDevolucao + ", valor_total="
				+ valor_total + ", dataDevolucao=" + dataDevolucao + ", devolvido=" + devolvido + "]";
	}
	
}