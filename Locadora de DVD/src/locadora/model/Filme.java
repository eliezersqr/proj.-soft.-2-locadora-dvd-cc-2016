package locadora.model;

public class Filme {

	private String nome;
	private String duracao;
	private String sinopse;
	private int quantidade;
	
	public Filme(String nome, String duracao, String sinopse, int quantidade) {
		super();
		this.nome = nome;
		this.duracao = duracao;
		this.sinopse = sinopse;
		this.quantidade = quantidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDuracao() {
		return duracao;
	}

	public void setDuracao(String duracao) {
		this.duracao = duracao;
	}

	public String getSinopse() {
		return sinopse;
	}

	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	@Override
	public String toString() {
		return "Filme [nome=" + nome + ", duracao=" + duracao + ", sinopse=" + sinopse + ", quantidade=" + quantidade
				+ "]";
	}
		
}