package locadora.model;

public class Cliente {
	
	private String nome;
	private int idade;
	private String sexo;
	private String endereco;
	private String bairro;
	private String cidade;
	private String dataInscricao;
	
	public Cliente(String nome, int idade, String sexo, String endereco, String bairro, String cidade,
			String dataInscricao) {
		super();
		this.nome = nome;
		this.idade = idade;
		this.sexo = sexo;
		this.endereco = endereco;
		this.bairro = bairro;
		this.cidade = cidade;
		this.dataInscricao = dataInscricao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getDataInscricao() {
		return dataInscricao;
	}

	public void setDataInscricao(String dataInscricao) {
		this.dataInscricao = dataInscricao;
	}

	@Override
	public String toString() {
		return "Cliente [nome=" + nome + ", idade=" + idade + ", sexo=" + sexo + ", endereco=" + endereco + ", bairro="
				+ bairro + ", cidade=" + cidade + ", dataInscricao=" + dataInscricao + "]";
	}
	
}