package locadora.model;

public class Generos_filme {

	private int cdGenero;
	private int cdFilme;
	
	public Generos_filme(int cdGenero, int cdFilme) {
		super();
		this.cdGenero = cdGenero;
		this.cdFilme = cdFilme;
	}

	public int getCdGenero() {
		return cdGenero;
	}

	public void setCdGenero(int cdGenero) {
		this.cdGenero = cdGenero;
	}

	public int getCdFilme() {
		return cdFilme;
	}

	public void setCdFilme(int cdFilme) {
		this.cdFilme = cdFilme;
	}

	@Override
	public String toString() {
		return "Generos_filme [cdGenero=" + cdGenero + ", cdFilme=" + cdFilme + "]";
	}
		
}