-- Gera��o de Modelo f�sico
-- Sql ANSI 2003 - brModelo.



CREATE TABLE StatusAtor (
cdStatus serial PRIMARY KEY,
status integer,
cdAtor integer,
cdFilme integer
);

CREATE TABLE Filme (
cdFilme serial PRIMARY KEY,
nome varchar(45),
duracao time,
sinopse varchar(200),
quantidade integer
);

CREATE TABLE Generos_Filme (
cdGeneroFilme serial PRIMARY KEY,
cdGenero integer,
cdFilme integer,
FOREIGN KEY(cdFilme) REFERENCES Filme (cdFilme)
);

CREATE TABLE Itens_Locacao (
num_seq integer,
cdLocacao integer,
cdFilme integer,
sub_total integer,
PRIMARY KEY(num_seq,cdLocacao),
FOREIGN KEY(cdFilme) REFERENCES Filme (cdFilme)
);

CREATE TABLE Locacao (
cdLocacao serial PRIMARY KEY,
dataLocacao date,
dataMaxDevolucao date,
valor_total integer,
dataDevolucao date,
devolvido integer,
cdCliente integer,
cdQuiosque integer
);

CREATE TABLE Quiosque (
cdQuiosque serial PRIMARY KEY,
endereco varchar(45),
bairro varchar(45),
cidade varchar(45),
bonus integer
);

CREATE TABLE Cliente (
cdCliente serial PRIMARY KEY,
nome varchar(45),
idade integer,
sexo varchar(15),
endereco varchar(50),
bairro varchar(45),
cidade varchar(45),
dataInscricao date,
cdCartao integer
);

CREATE TABLE Genero (
cdGenero serial PRIMARY KEY,
nome varchar(45)
);

CREATE TABLE Usuario_Adm (
cdUsuario serial PRIMARY KEY,
usuario varchar(15),
senha varchar(15)
);

CREATE TABLE Ator (
cdAtor serial PRIMARY KEY,
nome varchar(45)
);
	
CREATE TABLE Cartao (
cdCartao serial PRIMARY KEY,
tipoCliente varchar(45),
creditos integer
);

ALTER TABLE StatusAtor ADD FOREIGN KEY(cdAtor) REFERENCES Ator (cdAtor);
ALTER TABLE StatusAtor ADD FOREIGN KEY(cdFilme) REFERENCES Filme (cdFilme);
ALTER TABLE Generos_Filme ADD FOREIGN KEY(cdGenero) REFERENCES Genero (cdGenero);
ALTER TABLE Itens_Locacao ADD FOREIGN KEY(cdLocacao) REFERENCES Locacao (cdLocacao);
ALTER TABLE Locacao ADD FOREIGN KEY(cdCliente) REFERENCES Cliente (cdCliente);
ALTER TABLE Locacao ADD FOREIGN KEY(cdQuiosque) REFERENCES Quiosque (cdQuiosque);
ALTER TABLE Cliente ADD FOREIGN KEY(cdCartao) REFERENCES Cartao (cdCartao);