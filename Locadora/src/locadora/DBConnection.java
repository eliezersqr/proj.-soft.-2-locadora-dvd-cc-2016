package locadora;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	private static Connection connection = null;
	private static final String JDBC_DRIVER = "org.postgresql.Driver";
	private static String DATABASE_URL = "jdbc:postgresql://localhost:5432/locadora";
	private static String USR = "postgres";		//Usuario BD
	private static String PASS = "eliezers";	//Senha BD
	private static int connectionCount = 0;

	public static Connection getConnection() {
		if (connection == null) {
			try {
				Class.forName(JDBC_DRIVER);
				connection = DriverManager.getConnection(DATABASE_URL, USR, PASS);
			} catch (SQLException sqlException) {
				sqlException.printStackTrace();
				System.exit(1);
			} catch (ClassNotFoundException classNotFound) {
				classNotFound.printStackTrace();
				System.exit(1);
			}
		}
		connectionCount++;
		return connection;
	}

	public static boolean isConnected() {
		if (connection == null)
			return false;
		return true;
	}

	public static void closeConnection() throws IllegalStateException {
		if (connectionCount == 0)
			throw new IllegalStateException("Not Connected to Database");
		connectionCount--;
		if (connectionCount == 0) {
			try {
				connection.close();
			} catch (Exception exception) {
				exception.printStackTrace();
				System.exit(1);
			}
		}
	}
}