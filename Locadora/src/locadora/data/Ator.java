package locadora.data;

public class Ator {

	private int cdAtor;
	private String nome;

	public Ator() {
		this.cdAtor = 0;
		this.nome = "";
	}

	public Ator(String nome) {
		this.cdAtor = 0;
		this.nome = nome;
	}

	public Ator(int cdAtor, String nome) {
		this.cdAtor = cdAtor;
		this.nome = nome;
	}

	public int getCdAtor() {
		return cdAtor;
	}

	public void setCdAtor(int cdAtor) {
		this.cdAtor = cdAtor;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}