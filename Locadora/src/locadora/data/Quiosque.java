package locadora.data;

public class Quiosque {

	private int cdQuiosque;
	private String endereco;
	private String bairro;
	private String cidade;
	private int bonus;

	public Quiosque() {
		this.cdQuiosque = 0;
		this.endereco = "";
		this.bairro = "";
		this.cidade = "";
		this.bonus = 0;
	}

	public Quiosque(String endereco, String bairro, String cidade, int bonus) {
		this.cdQuiosque = 0;
		this.endereco = endereco;
		this.bairro = bairro;
		this.cidade = cidade;
		this.bonus = bonus;
	}

	public Quiosque(int cdQuiosque, String endereco, String bairro, String cidade, int bonus) {
		this.cdQuiosque = cdQuiosque;
		this.endereco = endereco;
		this.bairro = bairro;
		this.cidade = cidade;
		this.bonus = bonus;
	}

	public int getCdQuiosque() {
		return cdQuiosque;
	}

	public void setCdQuiosque(int cdQuiosque) {
		this.cdQuiosque = cdQuiosque;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public int getBonus() {
		return bonus;
	}

	public void setBonus(int bonus) {
		this.bonus = bonus;
	}
}