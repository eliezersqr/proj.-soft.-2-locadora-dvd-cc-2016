package locadora.data;

public class Itens_Locacao {

	private int num_seq;
	private int cdLocacao;
	private int cdFilme;
	private int sub_total;
	
	public Itens_Locacao() {
		this.num_seq = 0;
		this.cdLocacao = 0;
		this.cdFilme = 0;
		this.sub_total = 0;
	}
	
	public Itens_Locacao(int cdLocacao, int cdFilme, int sub_total) {
		this.num_seq = 0;
		this.cdLocacao = cdLocacao;
		this.cdFilme = cdFilme;
		this.sub_total = sub_total;
	}
	
	public Itens_Locacao(int num_seq, int cdLocacao, int cdFilme, int sub_total) {
		this.num_seq = num_seq;
		this.cdLocacao = cdLocacao;
		this.cdFilme = cdFilme;
		this.sub_total = sub_total;
	}

	public int getNum_seq() {
		return num_seq;
	}

	public void setNum_seq(int num_seq) {
		this.num_seq = num_seq;
	}

	public int getCdLocacao() {
		return cdLocacao;
	}

	public void setCdLocacao(int cdLocacao) {
		this.cdLocacao = cdLocacao;
	}

	public int getCdFilme() {
		return cdFilme;
	}

	public void setCdFilme(int cdFilme) {
		this.cdFilme = cdFilme;
	}

	public int getSub_total() {
		return sub_total;
	}

	public void setSub_total(int sub_total) {
		this.sub_total = sub_total;
	}
}