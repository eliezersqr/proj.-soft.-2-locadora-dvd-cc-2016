package locadora.data;

public class Generos_Filme {
	
	private int cdGenerosFilme;
	private int cdGenero;
	private int cdFilme;
	
	public Generos_Filme() {
		this.cdGenerosFilme = 0;
		this.cdGenero = 0;
		this.cdFilme = 0;
	}
	
	public Generos_Filme(int cdGenero, int cdFilme) {
		this.cdGenerosFilme = 0;
		this.cdGenero = cdGenero;
		this.cdFilme = cdFilme;
	}
	
	public Generos_Filme(int cdGenerosFilme, int cdGenero, int cdFilme) {
		this.cdGenerosFilme = cdGenerosFilme;
		this.cdGenero = cdGenero;
		this.cdFilme = cdFilme;
	}

	public int getCdGenerosFilme() {
		return cdGenerosFilme;
	}

	public void setCdGenerosFilme(int cdGenerosFilme) {
		this.cdGenerosFilme = cdGenerosFilme;
	}

	public int getCdGenero() {
		return cdGenero;
	}

	public void setCdGenero(int cdGenero) {
		this.cdGenero = cdGenero;
	}

	public int getCdFilme() {
		return cdFilme;
	}

	public void setCdFilme(int cdFilme) {
		this.cdFilme = cdFilme;
	}
}