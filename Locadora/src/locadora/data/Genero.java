package locadora.data;

public class Genero {

	private int cdGenero;
	private String nome;

	public Genero() {
		this.cdGenero = 0;
		this.nome = "";
	}

	
	public Genero(String nome) {
		this.cdGenero = 0;
		this.nome = nome;
	}

	public Genero(int cdGenero, String nome) {
		this.cdGenero = cdGenero;
		this.nome = nome;
	}

	public int getCdGenero() {
		return cdGenero;
	}

	public void setCdGenero(int cdGenero) {
		this.cdGenero = cdGenero;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}