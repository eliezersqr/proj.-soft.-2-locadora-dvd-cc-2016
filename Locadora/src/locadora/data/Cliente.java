package locadora.data;

public class Cliente {

	private int cdCliente;
	private String nome;
	private int idade;
	private String sexo;
	private String endereco;
	private String bairro;
	private String cidade;
	private String dataInscricao;
	private int cdCartao;

	public Cliente() {
		this.cdCliente = 0;
		this.nome = "";
		this.idade = 0;
		this.sexo = "";
		this.endereco = "";
		this.bairro = "";
		this.cidade = "";
		this.dataInscricao = "";
		this.cdCartao = 0;
	}

	public Cliente(String nome, int idade, String sexo, String endereco, String bairro, String cidade,
			String dataInscricao, int cdCartao) {
		this.cdCliente = 0;
		this.nome = nome;
		this.idade = idade;
		this.sexo = sexo;
		this.endereco = endereco;
		this.bairro = bairro;
		this.cidade = cidade;
		this.dataInscricao = dataInscricao;
		this.cdCartao = cdCartao;
	}

	public Cliente(int cdCliente, String nome, int idade, String sexo, String endereco, String bairro, String cidade,
			String dataInscricao, int cdCartao) {
		this.cdCliente = cdCliente;
		this.nome = nome;
		this.idade = idade;
		this.sexo = sexo;
		this.endereco = endereco;
		this.bairro = bairro;
		this.cidade = cidade;
		this.dataInscricao = dataInscricao;
		this.cdCartao = cdCartao;
	}

	public int getCdCliente() {
		return cdCliente;
	}

	public void setCdCliente(int cdCliente) {
		this.cdCliente = cdCliente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getDataInscricao() {
		return dataInscricao;
	}

	public void setDataInscricao(String dataInscricao) {
		this.dataInscricao = dataInscricao;
	}

	public int getCdCartao() {
		return cdCartao;
	}

	public void setCdCartao(int cdCartao) {
		this.cdCartao = cdCartao;
	}
}