package locadora.data;

public class StatusAtor {

	private int cdStatus;
	private int status;
	private int cdAtor;
	private int cdFilme;

	public StatusAtor() {
		this.cdStatus = 0;
		this.status = 0;
		this.cdAtor = 0;
		this.cdFilme = 0;
	}

	public StatusAtor(int status, int cdAtor, int cdFilme) {
		this.cdStatus = 0;
		this.status = status;
		this.cdAtor = cdAtor;
		this.cdFilme = cdFilme;
	}

	public StatusAtor(int cdStatus, int status, int cdAtor, int cdFilme) {
		this.cdStatus = cdStatus;
		this.status = status;
		this.cdAtor = cdAtor;
		this.cdFilme = cdFilme;
	}

	public int getCdStatus() {
		return cdStatus;
	}

	public void setCdStatus(int cdStatus) {
		this.cdStatus = cdStatus;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getCdAtor() {
		return cdAtor;
	}

	public void setCdAtor(int cdAtor) {
		this.cdAtor = cdAtor;
	}

	public int getCdFilme() {
		return cdFilme;
	}

	public void setCdFilme(int cdFilme) {
		this.cdFilme = cdFilme;
	}
}