package locadora.data;

public class Locacao {

	private int cdLocacao;
	private String dataLocacao;
	private String dataMaxDevolucao;
	private int valor_total;
	private String dataDevolucao;
	private int devolvido;
	private int cdCliente;
	private int cdQuiosque;

	public Locacao() {
		this.cdLocacao = 0;
		this.dataLocacao = "";
		this.dataMaxDevolucao = "";
		this.valor_total = 0;
		this.dataDevolucao = "";
		this.devolvido = 0;
		this.cdCliente = 0;
		this.cdQuiosque = 0;
	}

	public Locacao(String dataLocacao, String dataMaxDevolucao, int valor_total, String dataDevolucao, int devolvido,
			int cdCliente, int cdQuiosque) {
		this.cdLocacao = 0;
		this.dataLocacao = dataLocacao;
		this.dataMaxDevolucao = dataMaxDevolucao;
		this.valor_total = valor_total;
		this.dataDevolucao = dataDevolucao;
		this.devolvido = devolvido;
		this.cdCliente = cdCliente;
		this.cdQuiosque = cdQuiosque;
	}

	public Locacao(int cdLocacao, String dataLocacao, String dataMaxDevolucao, int valor_total, String dataDevolucao,
			int devolvido, int cdCliente, int cdQuiosque) {
		this.cdLocacao = cdLocacao;
		this.dataLocacao = dataLocacao;
		this.dataMaxDevolucao = dataMaxDevolucao;
		this.valor_total = valor_total;
		this.dataDevolucao = dataDevolucao;
		this.devolvido = devolvido;
		this.cdCliente = cdCliente;
		this.cdQuiosque = cdQuiosque;
	}

	public int getCdLocacao() {
		return cdLocacao;
	}

	public void setCdLocacao(int cdLocacao) {
		this.cdLocacao = cdLocacao;
	}

	public String getDataLocacao() {
		return dataLocacao;
	}

	public void setDataLocacao(String dataLocacao) {
		this.dataLocacao = dataLocacao;
	}

	public String getDataMaxDevolucao() {
		return dataMaxDevolucao;
	}

	public void setDataMaxDevolucao(String dataMaxDevolucao) {
		this.dataMaxDevolucao = dataMaxDevolucao;
	}

	public int getValor_total() {
		return valor_total;
	}

	public void setValor_total(int valor_total) {
		this.valor_total = valor_total;
	}

	public String getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(String dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public int getDevolvido() {
		return devolvido;
	}

	public void setDevolvido(int devolvido) {
		this.devolvido = devolvido;
	}

	public int getCdCliente() {
		return cdCliente;
	}

	public void setCdCliente(int cdCliente) {
		this.cdCliente = cdCliente;
	}

	public int getCdQuiosque() {
		return cdQuiosque;
	}

	public void setCdQuiosque(int cdQuiosque) {
		this.cdQuiosque = cdQuiosque;
	}
}