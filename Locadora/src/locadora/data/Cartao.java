package locadora.data;

public class Cartao {

	private int cdCartao;
	private String tipoCliente;
	private int creditos;

	public Cartao() {
		this.cdCartao = 0;
		this.tipoCliente = "";
		this.creditos = 0;
	}

	public Cartao(String tipoCliente, int creditos) {
		this.cdCartao = 0;
		this.tipoCliente = tipoCliente;
		this.creditos = creditos;
	}

	public Cartao(int cdCartao, String tipoCliente, int creditos) {
		this.cdCartao = cdCartao;
		this.tipoCliente = tipoCliente;
		this.creditos = creditos;
	}

	public int getCdCartao() {
		return cdCartao;
	}

	public void setCdCartao(int cdCartao) {
		this.cdCartao = cdCartao;
	}

	public String getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public int getCreditos() {
		return creditos;
	}

	public void setCreditos(int creditos) {
		this.creditos = creditos;
	}
}