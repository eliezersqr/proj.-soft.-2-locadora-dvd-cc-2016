package locadora.data;

public class Filme {

	private int cdFilme;
	private String nome;
	private String duracao;
	private String sinopse;
	private int quantidade;

	public Filme() {
		this.cdFilme = 0;
		this.nome = "";
		this.duracao = "";
		this.sinopse = "";
		this.quantidade = 0;
	}

	public Filme(int cdFilme, String nome, String duracao, String sinopse, int quantidade) {
		this.cdFilme = cdFilme;
		this.nome = nome;
		this.duracao = duracao;
		this.sinopse = sinopse;
		this.quantidade = quantidade;
	}

	public Filme(String nome, String duracao, String sinopse, int quantidade) {
		this.cdFilme = 0;
		this.nome = nome;
		this.duracao = duracao;
		this.sinopse = sinopse;
		this.quantidade = quantidade;
	}

	public int getCdFilme() {
		return cdFilme;
	}

	public void setCdFilme(int cdFilme) {
		this.cdFilme = cdFilme;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDuracao() {
		return duracao;
	}

	public void setDuracao(String duracao) {
		this.duracao = duracao;
	}

	public String getSinopse() {
		return sinopse;
	}

	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
}