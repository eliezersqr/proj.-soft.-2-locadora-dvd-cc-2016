package locadora.tableModel;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.table.AbstractTableModel;

import locadora.DBConnection;

public class FilmeTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private Statement statement;
	private ResultSet resultSet;
	private ResultSetMetaData metaData;
	private int numberOfRows;
	private String query = "SELECT * FROM Filme ORDER BY cdFilme";

	public FilmeTableModel() {
		try {

			statement = DBConnection.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			setQuery();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public Class<?> getColumnClass(int column) throws IllegalStateException {
		try {
			String className = metaData.getColumnClassName(column + 1);

			return Class.forName(className);
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return Object.class;
	}

	public int getColumnCount() throws IllegalStateException {

		try {
			return metaData.getColumnCount();
		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
		}

		return 0;

	}

	public String getColumnName(int column) throws IllegalStateException {

		try {
			return metaData.getColumnName(column + 1);
		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
		}

		return "";
	}

	public int getRowCount() throws IllegalStateException {
		return numberOfRows;
	}

	public Object getValueAt(int row, int column) throws IllegalStateException {

		try {
			resultSet.absolute(row + 1);
			return resultSet.getObject(column + 1);
		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
		}

		return "";
	}

	public void setQuery() throws SQLException, IllegalStateException {

		resultSet = statement.executeQuery(query);

		metaData = resultSet.getMetaData();

		resultSet.last();
		numberOfRows = resultSet.getRow();

		fireTableStructureChanged();
	}

	public void disconnectFromDatabase() {

		try {
			statement.close();
			DBConnection.getConnection().close();
		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
		}
	}
}