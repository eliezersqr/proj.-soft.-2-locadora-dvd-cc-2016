package locadora.tableModel;

import java.sql.SQLException;

import javax.swing.table.AbstractTableModel;

import locadora.DAO.LocacaoDAO;

public class LocacaoTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private static LocacaoDAO locacaoTable;

	public LocacaoTableModel() {
		locacaoTable = new LocacaoDAO();
	}

	public Class<?> getColumnClass(int column) {
		return LocacaoDAO.getColumnClassName(column + 1);
	}

	public int getColumnCount() {
		return LocacaoDAO.getNumColumns();
	}

	public String getColumnName(int column) {
		return LocacaoDAO.getColumnName(column);
	}

	public int getRowCount() throws IllegalStateException {
		return locacaoTable.getNumberOfRows();
	}

	public Object getValueAt(int row, int column) throws IllegalStateException {
		locacaoTable.moveToRow(row + 1);
		return locacaoTable.getColumn(column + 1);
	}

	public void newQuery() throws SQLException, IllegalStateException {
		locacaoTable.setQuery();

		fireTableStructureChanged();
	}
}