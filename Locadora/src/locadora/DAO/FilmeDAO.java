package locadora.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import locadora.DBConnection;
import locadora.data.Filme;

public class FilmeDAO {
	private Filme filme;
	private Statement statement;
	private ResultSet resultSet;

	private int numberOfRows;
	private String query = "SELECT * FROM Filme";
	private String update = "UPDATE (cdFilme, nome, duracao, sinopse, quantidade) from Filme with ";

	static private final String[] columnsNames = { "Codigo Filme", "Nome", "Duracao", "Sinopse", "Quantidade" };

	static private final int numColumns = 5;

	public FilmeDAO() {
		filme = new Filme();
		try {
			statement = DBConnection.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			setQuery();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public void moveToRow(int row) {
		try {
			resultSet.absolute(row);
			filme.setCdFilme(resultSet.getInt(1));
			filme.setNome(resultSet.getString(2));
			filme.setDuracao(resultSet.getString(3));
			filme.setSinopse(resultSet.getString(4));
			filme.setQuantidade(resultSet.getInt(5));
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public Filme getFilme() {
		return filme;
	}

	public int getNumberOfRows() {
		return numberOfRows;
	}

	static public int getNumColumns() {
		return numColumns;
	}

	static public Class<?> getColumnClassName(int column) {
		try {
			switch (column) {
			case 1:
				return Class.forName(Integer.class.getName());
			case 2:
				return Class.forName(String.class.getName());
			case 3:
				return Class.forName(String.class.getName());
			case 4:
				return Class.forName(String.class.getName());
			case 5:
				return Class.forName(Integer.class.getName());
			}
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		return Object.class;
	}

	public Object getColumn(int column) {
		switch (column) {
		case 1:
			return filme.getCdFilme();
		case 2:
			return filme.getNome();
		case 3:
			return filme.getDuracao();
		case 4:
			return filme.getSinopse();
		case 5:
			return filme.getQuantidade();
		}
		return null;
	}

	static public String getColumnName(int column) {
		if (column > 0 && column < columnsNames.length)
			return columnsNames[column];
		return "";
	}

	public void save() throws SQLException, IllegalStateException {
		String newData = update + "('" + filme.getCdFilme() + "', '" + filme.getNome() + "', '" + filme.getDuracao()
				+ "', '" + filme.getSinopse() + "', '" + filme.getQuantidade() + "');";
		statement.executeUpdate(newData);
	}

	public void setQuery() throws SQLException, IllegalStateException {

		resultSet = statement.executeQuery(query);
		resultSet.last();
		numberOfRows = resultSet.getRow();
		resultSet.first();

		filme.setCdFilme(resultSet.getInt(1));
		filme.setNome(resultSet.getString(2));
		filme.setDuracao(resultSet.getString(3));
		filme.setSinopse(resultSet.getString(4));
		filme.setQuantidade(resultSet.getInt(5));
	}

	public void disconnectFromDatabase() {
		try {
			statement.close();
			DBConnection.getConnection().close();
		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
		}
	}
}