package locadora.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import locadora.DBConnection;
import locadora.data.Locacao;

public class LocacaoDAO {

	private Locacao locacao;
	private Statement statement;
	private ResultSet resultSet;

	private int numberOfRows;
	private String query = "SELECT * FROM Locacao";
	private String update = "UPDATE (cdLocacao, dataLocacao, dataMaxDevolucao, valor_total, dataDevolucao, devolvido, cdCliente, cdQuiosque) from Locacao with ";

	static private final String[] columnsNames = { "Codigo Locacao", "Data da Locacao", "Data Max de Devolucao",
			"Valor Total", "Data da Devolucao", "Devolvido(0-nao/1-sim)", "Codigo do Cliente", "Codigo do Quiosque" };

	static private final int numColumns = 8;
	
	public LocacaoDAO()
	{
		locacao = new Locacao();
		try
		{
			statement = DBConnection.getConnection().createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			setQuery();
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
	}
	
	public void moveToRow(int row)
	{
		try
		{
			resultSet.absolute(row);
			locacao.setCdLocacao(resultSet.getInt(1));
			locacao.setDataLocacao(resultSet.getString(2));
			locacao.setDataMaxDevolucao(resultSet.getString(3));
			locacao.setValor_total(resultSet.getInt(4));
			locacao.setDataDevolucao(resultSet.getString(5));
			locacao.setDevolvido(resultSet.getInt(6));
			locacao.setCdCliente(resultSet.getInt(7));
			locacao.setCdQuiosque(resultSet.getInt(8));
			
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
	}
	
	public Locacao getLocacao()
	{
		return locacao;
	}

	public int getNumberOfRows()
	{
		return numberOfRows;
	}

	static public int getNumColumns()
	{
		return numColumns;
	}
	
	static public Class<?> getColumnClassName(int column)
	{
		try
		{
			switch(column)
			{
				case 1:
					return Class.forName(Integer.class.getName());
				case 2:
					return Class.forName(String.class.getName());
				case 3:
					return Class.forName(String.class.getName());
				case 4:
					return Class.forName(Integer.class.getName());
				case 5:
					return Class.forName(String.class.getName());
				case 6:
					return Class.forName(Integer.class.getName());
				case 7:
					return Class.forName(Integer.class.getName());
				case 8:
					return Class.forName(Integer.class.getName());
			}
		}
		catch(ClassNotFoundException ex)
		{
			ex.printStackTrace();
		}
		return Object.class; 
	}
	
	public Object getColumn(int column)
	{
		switch(column)
		{
			case 1:
				return locacao.getCdLocacao();
			case 2:
				return locacao.getDataLocacao();
			case 3:
				return locacao.getDataMaxDevolucao();
			case 4:
				return locacao.getValor_total();
			case 5:
				return locacao.getDataDevolucao();
			case 6:
				return locacao.getDevolvido();
			case 7:
				return locacao.getCdCliente();
			case 8:
				return locacao.getCdQuiosque();
			
		}
		return null;
	}
	
	static public String getColumnName(int column)
	{
		if(column > 0 || column < columnsNames.length)
			return columnsNames[column];
		return "";
	}
	
	public void save() throws SQLException, IllegalStateException
	{
		String newData = update + "('" + locacao.getCdLocacao() + "', '" +
			locacao.getDataLocacao() + "', '" + locacao.getDataMaxDevolucao() + "', '" + locacao.getValor_total() +
			"', '" + locacao.getDataDevolucao() + "', '" + locacao.getDevolvido() + "', '" + locacao.getCdCliente() + "', '" + locacao.getCdQuiosque() + "' );";
		statement.executeUpdate(newData);
	}
	

	public void setQuery() throws SQLException, IllegalStateException
	{

		resultSet = statement.executeQuery(query);
		resultSet.last();
		numberOfRows = resultSet.getRow();
		resultSet.first();

		locacao.setCdLocacao(resultSet.getInt(1));
		locacao.setDataLocacao(resultSet.getString(2));
		locacao.setDataMaxDevolucao(resultSet.getString(3));
		locacao.setValor_total(resultSet.getInt(4));
		locacao.setDataDevolucao(resultSet.getString(5));
		locacao.setDevolvido(resultSet.getInt(6));
		locacao.setCdCliente(resultSet.getInt(7));
		locacao.setCdQuiosque(resultSet.getInt(8));
	}
	
	public void disconnectFromDatabase()
	{
		try
		{
			statement.close();
			DBConnection.getConnection().close();
		}
		catch (SQLException sqlException)
		{
			sqlException.printStackTrace();
		}
	} 
}
