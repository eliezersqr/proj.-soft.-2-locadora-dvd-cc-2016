package locadora.GUI;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import locadora.tableModel.LocacaoTableModel;
import locadora.tableModel.FilmeTableModel;

/**
 * @author mdmatrakas
 * 
 */
public class DisplayTables extends JFrame {

	private static final long serialVersionUID = 1L;
	LocacaoTableModel locacaoTableModel;
	FilmeTableModel filmeTableModel;
	boolean coffeeSupplier = true;
	JTable resultTable;
	JButton locacaoButton;
	JButton filmeButton;

	public DisplayTables() {
		locacaoTableModel = new LocacaoTableModel();
		filmeTableModel = new FilmeTableModel();

		locacaoButton = new JButton("Locacoes Feitas");
		filmeButton = new JButton("Filmes");

		Box box = Box.createHorizontalBox();
		box.add(locacaoButton);
		box.add(filmeButton);
		locacaoButton.setEnabled(false);
		filmeButton.setEnabled(true);

		resultTable = new JTable(locacaoTableModel);

		add(box, BorderLayout.NORTH);
		add(new JScrollPane(resultTable), BorderLayout.CENTER);

		locacaoButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (coffeeSupplier)
					return;

				resultTable.setModel(locacaoTableModel);
				resultTable.setRowSelectionInterval(0, 0);
				locacaoButton.setEnabled(false);
				filmeButton.setEnabled(true);
				coffeeSupplier = !coffeeSupplier;

			}
		});

		filmeButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				if (!coffeeSupplier)
					return;

				resultTable.setModel(filmeTableModel);
				resultTable.setRowSelectionInterval(0, 0);
				locacaoButton.setEnabled(true);
				filmeButton.setEnabled(false);
				coffeeSupplier = !coffeeSupplier;

			}
		});

		resultTable.setRowSelectionInterval(0, 0);
		setSize(500, 250);
		setVisible(true);

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		addWindowListener(new WindowAdapter() {

			public void windowClosed(WindowEvent event) {

				filmeTableModel.disconnectFromDatabase();
				System.exit(0);
			}
		});
	}
}